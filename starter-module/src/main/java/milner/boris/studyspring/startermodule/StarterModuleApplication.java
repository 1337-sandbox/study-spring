package milner.boris.studyspring.startermodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "milner.boris.studyspring")
public class StarterModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarterModuleApplication.class, args);
    }

}
