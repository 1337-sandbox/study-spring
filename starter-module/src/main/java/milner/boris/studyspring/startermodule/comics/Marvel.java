package milner.boris.studyspring.startermodule.comics;

import milner.boris.studyspring.coremodule.interfaces.IHasSuperPowers;
import milner.boris.studyspring.heroesmodule.heroes.Hero;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Marvel {
    private static final Logger log = LoggerFactory.getLogger(Marvel.class);

    public Marvel(List<Hero> allAvailableHeroes) {
        log.debug("Marvel comics is created !");
        log.info("Classpath contains {} heroes: {}", allAvailableHeroes.size(), allAvailableHeroes);
        for (Hero hero : allAvailableHeroes) {
            log.debug("The hero {} has an id of {}", hero.getClass().getSimpleName(), hero.getId());
            if (hero.hasSuperpowers()) {
                log.warn("{} has super powers: {}", hero.getId(), ((IHasSuperPowers) hero).getSuperPowers());
            }
        }
    }
}
