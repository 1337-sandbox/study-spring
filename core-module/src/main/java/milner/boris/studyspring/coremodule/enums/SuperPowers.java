package milner.boris.studyspring.coremodule.enums;

public enum SuperPowers {
    Flying,
    LaserSight,
    ClimbingWalls,
    Strength,
    Invisibility,
    Magic
}
