package milner.boris.studyspring.coremodule.interfaces;

public interface Startable {
    void start();
}
