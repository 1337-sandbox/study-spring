package milner.boris.studyspring.coremodule.interfaces;

import milner.boris.studyspring.coremodule.enums.SuperPowers;

import java.util.List;

public interface IHasSuperPowers {
    List<SuperPowers> getSuperPowers();
}
