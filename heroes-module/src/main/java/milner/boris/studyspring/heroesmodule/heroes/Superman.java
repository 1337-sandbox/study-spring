package milner.boris.studyspring.heroesmodule.heroes;

import milner.boris.studyspring.coremodule.enums.SuperPowers;
import milner.boris.studyspring.coremodule.interfaces.IHasSuperPowers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class Superman extends Hero implements IHasSuperPowers {
    private static final Logger log = LoggerFactory.getLogger(Superman.class);

    protected Superman(@Value("${supermanId}") String id) {
        super(id);
        log.info("Superman is created !");
    }

    @Override
    public List<SuperPowers> getSuperPowers() {
        return Arrays.asList(SuperPowers.Flying, SuperPowers.Strength);
    }
}
