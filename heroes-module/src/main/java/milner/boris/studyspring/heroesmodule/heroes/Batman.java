package milner.boris.studyspring.heroesmodule.heroes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Batman extends Hero {
    private static final Logger log = LoggerFactory.getLogger(Batman.class);


    public Batman(@Value("${batmanId}") String id) {
        super(id);
        log.info("Batman is created !");
    }


}
