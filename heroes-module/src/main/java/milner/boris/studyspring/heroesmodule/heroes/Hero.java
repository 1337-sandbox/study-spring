package milner.boris.studyspring.heroesmodule.heroes;


import milner.boris.studyspring.coremodule.interfaces.IHasSuperPowers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Hero {
    private static final Logger log = LoggerFactory.getLogger(Hero.class);
    private final String id;

    protected Hero(String id) {
        this.id = id;
        log.debug("Hello, I'm {} of class {}", id, this.getClass().getSimpleName());
    }

    public boolean hasSuperpowers() {
        Class<?>[] interfaces = this.getClass().getInterfaces();
        for (Class<?> anInterface : interfaces) {
            if (anInterface == IHasSuperPowers.class) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return this.id;
    }

    public String getId() {
        return id;
    }
}
